// PROBLEM 1 = check bilangan prima

function primeNumbers(n) {
	if (n === 1 || n === 2) {
  	return 'Bilangan Prima'
  } else {
      for(let i = 2; i < n; i++) {
        if(n % i === 0) {
            return 'Bukan Bilangan Prima'
        } else {
            return 'Bilangan Prima'
        }
    }
  }

}
// testing dengan input 13
console.log(primeNumbers(13))

// PROBLEM 2 = check saklar lampu

function matiLampu(N) {
    // kalo jumlah lampu ganjil berarti nyala, counter lampu ditambah ketika N habis dibagi 1
    var lampu = 0;
        for(let i = 1; i <= N; i++) {
          if (N % i === 0) {
            lampu = lampu+1;
        }
      }
      if (lampu % 2 === 0) {
            return 'lampu mati'
      } else {
          return 'lampu nyala'
      }
    }
    // testing dengan input 4
    console.log(matiLampu(4));
